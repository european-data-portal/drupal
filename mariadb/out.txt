NAME: mdb
LAST DEPLOYED: Thu Apr  9 14:20:15 2020
NAMESPACE: prod
STATUS: deployed
REVISION: 1
NOTES:
Please be patient while the chart is being deployed

Tip:

  Watch the deployment status using the command: kubectl get pods -w --namespace prod -l release=mdb

Services:

  echo Master: mdb-mariadb.prod.svc.cluster.local:3306
  echo Slave:  mdb-mariadb-slave.prod.svc.cluster.local:3306

Administrator credentials:

  Username: root
  Password : $(kubectl get secret --namespace prod mdb-mariadb -o jsonpath="{.data.mariadb-root-password}" | base64 --decode)

To connect to your database:

  1. Run a pod that you can use as a client:

      kubectl run mdb-mariadb-client --rm --tty -i --restart='Never' --image  docker.io/bitnami/mariadb:10.3.22-debian-10-r60 --namespace prod --command -- bash

  2. To connect to master service (read/write):

      mysql -h mdb-mariadb.prod.svc.cluster.local -uroot -p drupal

  3. To connect to slave service (read-only):

      mysql -h mdb-mariadb-slave.prod.svc.cluster.local -uroot -p drupal

To upgrade this helm chart:

  1. Obtain the password as described on the 'Administrator credentials' section and set the 'rootUser.password' parameter as shown below:

      ROOT_PASSWORD=$(kubectl get secret --namespace prod mdb-mariadb -o jsonpath="{.data.mariadb-root-password}" | base64 --decode)
      helm upgrade mdb bitnami/mariadb --set rootUser.password=$ROOT_PASSWORD

