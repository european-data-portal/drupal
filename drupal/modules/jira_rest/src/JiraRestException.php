<?php

namespace Drupal\jira_rest;

use Exception;

/**
 * Class JiraRestException.
 *
 * @package Drupal\jira_rest
 */
class JiraRestException extends Exception {

}
