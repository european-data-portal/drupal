var conTerraMapsLoaded = false;

(function ($, Drupal, drupalSettings) {

	Drupal.behaviors.myDashboardBehavior = {
		attach: function (context, settings) {

			loadConTerraMaps($);

		}
	};

})(jQuery, Drupal, drupalSettings);

function loadConTerraMaps($) {

	if ( conTerraMapsLoaded ) {
		return;
	}

	conTerraMapsLoaded = true;

	$.each( $("div.conterra-map"), function(k,v) {
		var thisId = $(this).attr("id");
		$apprt.load(function (Launcher) {
			new Launcher({
				configLocation: "builderapps"
			}).launchApp(thisId,thisId)
		});
	});

}