var map_data;

(function ($, Drupal, drupalSettings) {

	Drupal.behaviors.myDashboardBehavior = {
		attach: function (context, settings) {

			odmDashboard($);

		}
	};

})(jQuery, Drupal, drupalSettings);

function odmDashboard($) {

	if ( ! $("pre#json-data").length || $("pre#json-data").text().trim() === "" ) {
		return;
	}

	json = JSON.parse( $("pre#json-data").text() );

	$("pre#json-data").empty();

	u = location.pathname.split("/");
	year = u[ u.length-1 ];
	$('#block-opendatamaturitydashboards-2 a[href$="' + year + '"]').addClass('is-active');

	// overview
		if ( json.overview.policy ) {
			// policy
				$("#progress-overview-policy-framework strong").text( json.overview.policy.framework + "%" );
				$("#progress-overview-policy-framework .progress-bar").css("width",json.overview.policy.framework + "%" );
				if ( json.overview.policy.coordination ) {
					$("#progress-overview-policy-coordination strong").text( json.overview.policy.coordination + "%" );
					$("#progress-overview-policy-coordination .progress-bar").css("width",json.overview.policy.coordination + "%" );
					$("#progress-overview-policy-licencing strong").text( json.overview.policy.licencing + "%" );
					$("#progress-overview-policy-licencing .progress-bar").css("width",json.overview.policy.licencing + "%" );
				} else {
					$("#progress-overview-policy-governance strong").text( json.overview.policy.governance + "%" );
					$("#progress-overview-policy-governance .progress-bar").css("width",json.overview.policy.governance + "%" );
					$("#progress-overview-policy-implementation strong").text( json.overview.policy.implementation + "%" );
					$("#progress-overview-policy-implementation .progress-bar").css("width",json.overview.policy.implementation + "%" );					
				}
				$("#progress-overview-policy-total strong").text( json.overview.policy.total + "%" );
				$("#progress-overview-policy-total .progress-bar").css("width",json.overview.policy.total + "%" );
			// portals
				$("#progress-overview-portals-features strong").text( json.overview.portal.features + "%" );
				$("#progress-overview-portals-features .progress-bar").css("width",json.overview.portal.features + "%" );
				$("#progress-overview-portals-usage strong").text( json.overview.portal.usage + "%" );
				$("#progress-overview-portals-usage .progress-bar").css("width",json.overview.portal.usage + "%" );
				$("#progress-overview-portals-provision strong").text( json.overview.portal.provision + "%" );
				$("#progress-overview-portals-provision .progress-bar").css("width",json.overview.portal.provision + "%" );
				$("#progress-overview-portals-sustain strong").text( json.overview.portal.sustain + "%" );
				$("#progress-overview-portals-sustain .progress-bar").css("width",json.overview.portal.sustain + "%" );
				$("#progress-overview-portals-total strong").text( json.overview.portal.total + "%" );
				$("#progress-overview-portals-total .progress-bar").css("width",json.overview.portal.total + "%" );
			// impact
				$("#progress-overview-impact-aware strong").text( json.overview.impact.aware + "%" );
				$("#progress-overview-impact-aware .progress-bar").css("width",json.overview.impact.aware + "%" );
				$("#progress-overview-impact-political strong").text( json.overview.impact.political + "%" );
				$("#progress-overview-impact-political .progress-bar").css("width",json.overview.impact.political + "%" );
				$("#progress-overview-impact-social strong").text( json.overview.impact.social + "%" );
				$("#progress-overview-impact-social .progress-bar").css("width",json.overview.impact.social + "%" );
				$("#progress-overview-impact-environment strong").text( json.overview.impact.environment + "%" );
				$("#progress-overview-impact-environment .progress-bar").css("width",json.overview.impact.environment + "%" );
				$("#progress-overview-impact-economic strong").text( json.overview.impact.economic + "%" );
				$("#progress-overview-impact-economic .progress-bar").css("width",json.overview.impact.economic + "%" );
				$("#progress-overview-impact-total strong").text( json.overview.impact.total + "%" );
				$("#progress-overview-impact-total .progress-bar").css("width",json.overview.impact.total + "%" );
			// quality
				if ( json.overview.quality.automation ) {
					$("#progress-overview-quality-automation strong").text( json.overview.quality.automation + "%" );
					$("#progress-overview-quality-automation .progress-bar").css("width",json.overview.quality.automation + "%" );
					$("#progress-overview-quality-currency strong").text( json.overview.quality.currency + "%" );
					$("#progress-overview-quality-currency .progress-bar").css("width",json.overview.quality.currency + "%" );
				} else {					
					$("#progress-overview-quality-monitoring strong").text( json.overview.quality.monitoring + "%" );
					$("#progress-overview-quality-monitoring .progress-bar").css("width",json.overview.quality.monitoring + "%" );
					$("#progress-overview-quality-currencycomplete strong").text( json.overview.quality.currencycomplete + "%" );
					$("#progress-overview-quality-currencycomplete .progress-bar").css("width",json.overview.quality.currencycomplete + "%" );
				}
				$("#progress-overview-quality-dcat strong").text( json.overview.quality.dcat + "%" );
				$("#progress-overview-quality-dcat .progress-bar").css("width",json.overview.quality.dcat + "%" );
				$("#progress-overview-quality-deploy strong").text( json.overview.quality.deploy + "%" );
				$("#progress-overview-quality-deploy .progress-bar").css("width",json.overview.quality.deploy + "%" );
				$("#progress-overview-quality-total strong").text( json.overview.quality.total + "%" );
				$("#progress-overview-quality-total .progress-bar").css("width",json.overview.quality.total + "%" );
		} else {
			// policy and use
				$("#progress-overview-ready-presence strong").text( json.overview.ready.presence.now + "%" );
				$("#progress-overview-ready-presence .progress-bar").css("width",json.overview.ready.presence.now + "%" );
				$("#progress-overview-ready-natcoord strong").text( json.overview.ready.natcoord.now + "%" );
				$("#progress-overview-ready-natcoord .progress-bar").css("width",json.overview.ready.natcoord.now + "%" );
				$("#progress-overview-ready-licensing strong").text( json.overview.ready.licensing.now + "%" );
				$("#progress-overview-ready-licensing .progress-bar").css("width",json.overview.ready.licensing.now + "%" );
				$("#progress-overview-ready-use strong").text( json.overview.ready.use.now + "%" );
				$("#progress-overview-ready-use .progress-bar").css("width",json.overview.ready.use.now + "%" );
			// portal maturity
				$("#progress-overview-maturity-usability strong").text( json.overview.maturity.usability.now + "%" );
				$("#progress-overview-maturity-usability .progress-bar").css("width",json.overview.maturity.usability.now + "%" );
				$("#progress-overview-maturity-reuse strong").text( json.overview.maturity.reuse.now + "%" );
				$("#progress-overview-maturity-reuse .progress-bar").css("width",json.overview.maturity.reuse.now + "%" );
				$("#progress-overview-maturity-spread strong").text( json.overview.maturity.spread.now + "%" );
				$("#progress-overview-maturity-spread .progress-bar").css("width",json.overview.maturity.spread.now + "%" );
			// portal maturity
				$("#progress-overview-impact-political strong").text( json.overview.impact.political.now + "%" );
				$("#progress-overview-impact-political .progress-bar").css("width",json.overview.impact.political.now + "%" );
				$("#progress-overview-impact-social strong").text( json.overview.impact.social.now + "%" );
				$("#progress-overview-impact-social .progress-bar").css("width",json.overview.impact.social.now + "%" );
				$("#progress-overview-impact-economic strong").text( json.overview.impact.economic.now + "%" );
				$("#progress-overview-impact-economic .progress-bar").css("width",json.overview.impact.economic.now + "%" );
			// total pies
				$(".overview-totals .pie.dashboard-ready h3 span").html(json.overview.ready.total+"%");
				$(".overview-totals .pie.dashboard-maturity h3 span").html(json.overview.maturity.total+"%");
				$(".overview-totals .pie.dashboard-ready canvas").remove();
				$(".overview-totals .pie.dashboard-ready iframe").remove();
				$(".overview-totals .pie.dashboard-maturity canvas").remove();
				$(".overview-totals .pie.dashboard-maturity iframe").remove();

				$(".overview-totals .pie.dashboard-ready .pie-wrapper").append('<canvas height="100px" id="readiness" width="100px"></canvas>');
				var ctx_readiness = $("#readiness");
				var pie_readiness = new Chart(ctx_readiness,{
					type:"doughnut",
					data: {
						labels: ["Readiness","Remaining"],
						datasets: [{
							data:[json.overview.ready.total,100-json.overview.ready.total],
							backgroundColor: ["#0065a2"],
							hoverBackgroundColor: ["#0065a2"]
						}]
					},
					options: {
						legend: { display: false },
					},
				});
				$(".overview-totals .pie.dashboard-maturity .pie-wrapper").append('<canvas height="100px" id="maturity" width="100px"></canvas>');
				var ctx_maturity= $("#maturity");
				var pie_readiness = new Chart(ctx_maturity,{
					type:"doughnut",
					data: {
						labels: ["Maturity","Remaining"],
						datasets: [{
							data:[json.overview.maturity.total,100-json.overview.maturity.total],
							backgroundColor: ["#FE9900"],
							hoverBackgroundColor: ["#FE9900"]
						}]
					},
					options: {
						legend: { display: false },
					}
				});
		}

	// country overview
		var graphOptions = {
			scales: {
				xAxes: [{
					stacked: true,
					ticks: { autoSkip: false },
					barThickness: 20,
					gridLines: { display: false }
				}],
				yAxes: [{
					stacked: true,
					ticks: { autoSkip: false },
					scaleLabel: { display: true, labelString: "Score" }
				}]
			},
			maintainAspectRatio: false
		};
		var graphLabels = [];

		var graphPolicy = [];
		var graphPortal = [];
		var graphImpact = [];
		var graphQuality = [];
		var graphMaturity = [];

		$.each(json.country,function(k,v){
			if ( k !== "Maximum" && k !== "EU28 Avg.") {
				graphLabels.push(k);
				if ( year > 2017 ) {
					v.policy.total !== -1 ? graphPolicy.push(v.policy.total) : graphPolicy.push(0);
					v.portal.total !== -1 ? graphPortal.push(v.portal.total) : graphPortal.push(0);
					v.impact.total !== -1 ? graphImpact.push(v.impact.total) : graphImpact.push(0);
					v.quality.total !== -1 ? graphQuality.push(v.quality.total) : graphQuality.push(0);
				} else {
					v.policy.total !== -1 ? graphPolicy.push(v.policy.total) : graphPolicy.push(0);
					v.maturity.total !== -1 ? graphMaturity.push(v.maturity.total) : graphMaturity.push(0);
					v.impact.total !== -1 ? graphImpact.push(v.impact.total) : graphImpact.push(0);
				}
			}
		});
		
		var theDatasets = [];
		if ( year > 2017 ) {
			theDatasets = [
				{ label: "Policy", data: graphPolicy, backgroundColor: "#039ECE" },
				{ label: "Portal", data: graphPortal, backgroundColor: "#202272" },
				{ label: "Impact", data: graphImpact, backgroundColor: "#FE5D20" },
				{ label: "Quality", data: graphQuality, backgroundColor: "#FE9900" }
			];
		} else {
			theDatasets = [
				{ label: "Open Data Readiness - Policy and Use", data: graphPolicy, backgroundColor: "#0065a2" },
				{ label: "Portal Maturity", data: graphMaturity, backgroundColor: "#FE9900" },
				{ label: "Open Data Readiness - Impact", data: graphImpact, backgroundColor: "#8f0100" }
			];
		}

		var ctx_overview = jQuery("#overview");
		var myChart = new Chart(ctx_overview, {
			type: 'bar',
			data: {
				labels: graphLabels,
				datasets: theDatasets
			},
			options: graphOptions
		});

	// table
		$.each(json.country,function(k,v) {
			if ( year > 2019 ) {
				$(".dashboard-table table tbody").append(
					$("<tr></tr>")
						.append($("<td></td>").html(
								v.file 	? "<a href='/sites/default/files/"+v.file+"' target='_blank'>"+k+"</a>" : k
							)
						)
						.append($("<td></td>").html( minusOneOrNoData(v.policy.framework) ))
						.append($("<td></td>").html( minusOneOrNoData(v.policy.governance) ))
						.append($("<td></td>").html( minusOneOrNoData(v.policy.implementation) ))
						.append($("<td></td>").html( minusOneOrNoData(v.portal.features) ))
						.append($("<td></td>").html( minusOneOrNoData(v.portal.usage) ))
						.append($("<td></td>").html( minusOneOrNoData(v.portal.provision) ))
						.append($("<td></td>").html( minusOneOrNoData(v.portal.sustain) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.aware) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.political) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.social) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.environment) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.economic) ))
						.append($("<td></td>").html( minusOneOrNoData(v.quality.monitoring) ))
						.append($("<td></td>").html( minusOneOrNoData(v.quality.currencycomplete) ))
						.append($("<td></td>").html( minusOneOrNoData(v.quality.dcat) ))
						.append($("<td></td>").html( minusOneOrNoData(v.quality.deploy) ))
						.append($("<td></td>").addClass("dashboard-policy").html(
							(v.policy.total !== -1 ?  "<div class='progress'><div aria-valuemax='"+json.country.Maximum.policy.total+"' aria-valuemin='0' aria-valuenow='"+v.policy.total+"' class='progress-bar' role='progressbar' style='width:"+((v.policy.total/json.country.Maximum.policy.total)*100)+"%''><span>"+v.policy.total+"</span></div></div>" : "")
						)).append($("<td></td>").addClass("dashboard-portals").html(
							(v.portal.total !== -1 ?  "<div class='progress'><div aria-valuemax='"+json.country.Maximum.portal.total+"' aria-valuemin='0' aria-valuenow='"+v.portal.total+"' class='progress-bar' role='progressbar' style='width:"+((v.portal.total/json.country.Maximum.portal.total)*100)+"%''><span>"+v.portal.total+"</span></div></div>" : "")
						)).append($("<td></td>").addClass("dashboard-impact").html(
							(v.impact.total !== -1 ?  "<div class='progress'><div aria-valuemax='"+json.country.Maximum.impact.total+"' aria-valuemin='0' aria-valuenow='"+v.impact.total+"' class='progress-bar' role='progressbar' style='width:"+((v.impact.total/json.country.Maximum.impact.total)*100)+"%''><span>"+v.impact.total+"</span></div></div>" : "")
						)).append($("<td></td>").addClass("dashboard-quality").html(
							(v.quality.total !== -1 ?  "<div class='progress'><div aria-valuemax='"+json.country.Maximum.quality.total+"' aria-valuemin='0' aria-valuenow='"+v.quality.total+"' class='progress-bar' role='progressbar' style='width:"+((v.quality.total/json.country.Maximum.quality.total)*100)+"%''><span>"+v.quality.total+"</span></div></div>" : "")
						))
						.append($("<td></td>").html( v.total + (k !== "Maximum" ? "%" : "") ))
				);
			} else if ( year > 2017 ) {
				$(".dashboard-table table tbody").append(
					$("<tr></tr>")
						.append($("<td></td>").html(
								v.file 	? "<a href='/sites/default/files/"+v.file+"' target='_blank'>"+k+"</a>" : k
							)
						)
						.append($("<td></td>").html( minusOneOrNoData(v.policy.framework) ))
						.append($("<td></td>").html( minusOneOrNoData(v.policy.coordination) ))
						.append($("<td></td>").html( minusOneOrNoData(v.policy.licencing) ))
						.append($("<td></td>").html( minusOneOrNoData(v.portal.features) ))
						.append($("<td></td>").html( minusOneOrNoData(v.portal.usage) ))
						.append($("<td></td>").html( minusOneOrNoData(v.portal.provision) ))
						.append($("<td></td>").html( minusOneOrNoData(v.portal.sustain) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.aware) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.political) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.social) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.environment) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.economic) ))
						.append($("<td></td>").html( minusOneOrNoData(v.quality.automation) ))
						.append($("<td></td>").html( minusOneOrNoData(v.quality.currency) ))
						.append($("<td></td>").html( minusOneOrNoData(v.quality.dcat) ))
						.append($("<td></td>").html( minusOneOrNoData(v.quality.deploy) ))
						.append($("<td></td>").addClass("dashboard-policy").html(
							(v.policy.total !== -1 ?  "<div class='progress'><div aria-valuemax='"+json.country.Maximum.policy.total+"' aria-valuemin='0' aria-valuenow='"+v.policy.total+"' class='progress-bar' role='progressbar' style='width:"+((v.policy.total/json.country.Maximum.policy.total)*100)+"%''><span>"+v.policy.total+"</span></div></div>" : "")
						)).append($("<td></td>").addClass("dashboard-portals").html(
							(v.portal.total !== -1 ?  "<div class='progress'><div aria-valuemax='"+json.country.Maximum.portal.total+"' aria-valuemin='0' aria-valuenow='"+v.portal.total+"' class='progress-bar' role='progressbar' style='width:"+((v.portal.total/json.country.Maximum.portal.total)*100)+"%''><span>"+v.portal.total+"</span></div></div>" : "")
						)).append($("<td></td>").addClass("dashboard-impact").html(
							(v.impact.total !== -1 ?  "<div class='progress'><div aria-valuemax='"+json.country.Maximum.impact.total+"' aria-valuemin='0' aria-valuenow='"+v.impact.total+"' class='progress-bar' role='progressbar' style='width:"+((v.impact.total/json.country.Maximum.impact.total)*100)+"%''><span>"+v.impact.total+"</span></div></div>" : "")
						)).append($("<td></td>").addClass("dashboard-quality").html(
							(v.quality.total !== -1 ?  "<div class='progress'><div aria-valuemax='"+json.country.Maximum.quality.total+"' aria-valuemin='0' aria-valuenow='"+v.quality.total+"' class='progress-bar' role='progressbar' style='width:"+((v.quality.total/json.country.Maximum.quality.total)*100)+"%''><span>"+v.quality.total+"</span></div></div>" : "")
						))
						.append($("<td></td>").html( v.total + (k !== "Maximum" ? "%" : "") ))
				);
			} else {
				$(".dashboard-table table tbody").append(
					$("<tr></tr>")
						.append($("<td></td>").html(
								v.file 	? "<a href='/sites/default/files/"+v.file+"' target='_blank'>"+k+"</a>" : k
							)
						)
						.append($("<td></td>").html( minusOneOrNoData(v.policy.presence) ))
						.append($("<td></td>").html( minusOneOrNoData(v.policy.natcoord) ))
						.append($("<td></td>").html( minusOneOrNoData(v.policy.licensing) ))
						.append($("<td></td>").html( minusOneOrNoData(v.policy.use) ))
						.append($("<td></td>").html( minusOneOrNoData(v.maturity.usability) ))
						.append($("<td></td>").html( minusOneOrNoData(v.maturity.reusability) ))
						.append($("<td></td>").html( minusOneOrNoData(v.maturity.spread) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.political) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.social) ))
						.append($("<td></td>").html( minusOneOrNoData(v.impact.economic) ))
						.append($("<td></td>").addClass("dashboard-ready").html(
							(v.policy.total !== -1 ? "<div class='progress'><div aria-valuemax='"+json.country.Maximum.policy.total+"' aria-valuemin='0' aria-valuenow='"+v.policy.total+"' class='progress-bar' role='progressbar' style='width:"+((v.policy.total/json.country.Maximum.policy.total)*100)+"%''><span>"+v.policy.total+"</span></div></div>" : "")
						))
						.append($("<td></td>").addClass("dashboard-maturity").html(
							(v.maturity.total !== -1 ? "<div class='progress'><div aria-valuemax='"+json.country.Maximum.maturity.total+"' aria-valuemin='0' aria-valuenow='"+v.maturity.total+"' class='progress-bar' role='progressbar' style='width:"+((v.maturity.total/json.country.Maximum.maturity.total)*100)+"%''><span>"+v.maturity.total+"</span></div></div>" : "")
						))
						.append($("<td></td>").addClass("dashboard-impact-old").html(
							(v.impact.total !== -1 ? "<div class='progress'><div aria-valuemax='"+json.country.Maximum.impact.total+"' aria-valuemin='0' aria-valuenow='"+v.impact.total+"' class='progress-bar' role='progressbar' style='width:"+((v.impact.total/json.country.Maximum.impact.total)*100)+"%''><span>"+v.impact.total+"</span></div></div>" : "")
						))
						.append($("<td></td>").html( v.total + (k !== "Maximum" ? "%" : "") ))
				);
			}
		});

	// map
		map_data = {
			AUT:	{ fillKey: json.country["Austria"].trend, level: json.country["Austria"].trend, val: json.country["Austria"].total, url: "/sites/default/files/"+json.country["Austria"].file },
			BEL:	{ fillKey: json.country["Belgium"].trend, level: json.country["Belgium"].trend, val: json.country["Belgium"].total, url: "/sites/default/files/"+json.country["Belgium"].file },
			BGR:	{ fillKey: json.country["Bulgaria"].trend, level: json.country["Bulgaria"].trend, val: json.country["Bulgaria"].total, url: "/sites/default/files/"+json.country["Bulgaria"].file },
			HRV:	{ fillKey: json.country["Croatia"].trend, level: json.country["Croatia"].trend, val: json.country["Croatia"].total, url: "/sites/default/files/"+json.country["Croatia"].file },
			CYP:	{ fillKey: json.country["Cyprus"].trend, level: json.country["Cyprus"].trend, val: json.country["Cyprus"].total, url: "/sites/default/files/"+json.country["Cyprus"].file },
			CZE:	{ fillKey: json.country["Czech Republic"].trend, level: json.country["Czech Republic"].trend, val: json.country["Czech Republic"].total, url: "/sites/default/files/"+json.country["Czech Republic"].file },
			DNK:	{ fillKey: json.country["Denmark"].trend, level: json.country["Denmark"].trend, val: json.country["Denmark"].total, url: "/sites/default/files/"+json.country["Denmark"].file },
			EST:	{ fillKey: json.country["Estonia"].trend, level: json.country["Estonia"].trend, val: json.country["Estonia"].total, url: "/sites/default/files/"+json.country["Estonia"].file },
			FIN:	{ fillKey: json.country["Finland"].trend, level: json.country["Finland"].trend, val: json.country["Finland"].total, url: "/sites/default/files/"+json.country["Finland"].file },
			ALA:	{ fillKey: json.country["Finland"].trend, level: json.country["Finland"].trend, val: json.country["Finland"].total, url: "/sites/default/files/"+json.country["Finland"].file },
			FRA:	{ fillKey: json.country["France"].trend, level: json.country["France"].trend, val: json.country["France"].total, url: "/sites/default/files/"+json.country["France"].file },
			DEU:	{ fillKey: json.country["Germany"].trend, level: json.country["Germany"].trend, val: json.country["Germany"].total, url: "/sites/default/files/"+json.country["Germany"].file },
			GRC:	{ fillKey: json.country["Greece"].trend, level: json.country["Greece"].trend, val: json.country["Greece"].total, url: "/sites/default/files/"+json.country["Greece"].file },
			HUN:	{ fillKey: "", level: "", val: "No data", url: "" },
			ISL:	{ fillKey: json.country["Iceland"].trend, level: json.country["Iceland"].trend, val: json.country["Iceland"].total, url: "/sites/default/files/"+json.country["Iceland"].file },
			IRL:	{ fillKey: json.country["Ireland"].trend, level: json.country["Ireland"].trend, val: json.country["Ireland"].total, url: "/sites/default/files/"+json.country["Ireland"].file },
			ITA:	{ fillKey: json.country["Italy"].trend, level: json.country["Italy"].trend, val: json.country["Italy"].total, url: "/sites/default/files/"+json.country["Italy"].file },
			LVA:	{ fillKey: json.country["Latvia"].trend, level: json.country["Latvia"].trend, val: json.country["Latvia"].total, url: "/sites/default/files/"+json.country["Latvia"].file },
			LIE:	{ fillKey: json.country["Liechtenstein"].trend, level: json.country["Liechtenstein"].trend, val: json.country["Liechtenstein"].total, url: "/sites/default/files/"+json.country["Liechtenstein"].file },
			LTU:	{ fillKey: json.country["Lithuania"].trend, level: json.country["Lithuania"].trend, val: json.country["Lithuania"].total, url: "/sites/default/files/"+json.country["Lithuania"].file },
			LUX:	{ fillKey: json.country["Luxembourg"].trend, level: json.country["Luxembourg"].trend, val: json.country["Luxembourg"].total, url: "/sites/default/files/"+json.country["Luxembourg"].file },
			MLT:	{ fillKey: json.country["Malta"].trend, level: json.country["Malta"].trend, val: json.country["Malta"].total, url: "/sites/default/files/"+json.country["Malta"].file },
			NLD:	{ fillKey: json.country["Netherlands"].trend, level: json.country["Netherlands"].trend, val: json.country["Netherlands"].total, url: "/sites/default/files/"+json.country["Netherlands"].file },
			NOR:	{ fillKey: json.country["Norway"].trend, level: json.country["Norway"].trend, val: json.country["Norway"].total, url: "/sites/default/files/"+json.country["Norway"].file },
			POL:	{ fillKey: json.country["Poland"].trend, level: json.country["Poland"].trend, val: json.country["Poland"].total, url: "/sites/default/files/"+json.country["Poland"].file },
			PRT:	{ fillKey: json.country["Portugal"].trend, level: json.country["Portugal"].trend, val: json.country["Portugal"].total, url: "/sites/default/files/"+json.country["Portugal"].file },
			ROU:	{ fillKey: json.country["Romania"].trend, level: json.country["Romania"].trend, val: json.country["Romania"].total, url: "/sites/default/files/"+json.country["Romania"].file },
			SVK:	{ fillKey: json.country["Slovakia"].trend, level: json.country["Slovakia"].trend, val: json.country["Slovakia"].total, url: "/sites/default/files/"+json.country["Slovakia"].file },
			SVN:	{ fillKey: json.country["Slovenia"].trend, level: json.country["Slovenia"].trend, val: json.country["Slovenia"].total, url: "/sites/default/files/"+json.country["Slovenia"].file },
			ESP:	{ fillKey: json.country["Spain"].trend, level: json.country["Spain"].trend, val: json.country["Spain"].total, url: "/sites/default/files/"+json.country["Spain"].file },
			SWE:	{ fillKey: json.country["Sweden"].trend, level: json.country["Sweden"].trend, val: json.country["Sweden"].total, url: "/sites/default/files/"+json.country["Sweden"].file },
			CHE:	{ fillKey: json.country["Switzerland"].trend, level: json.country["Switzerland"].trend, val: json.country["Switzerland"].total, url: "/sites/default/files/"+json.country["Switzerland"].file },
			GBR:	{ fillKey: json.country["United Kingdom"].trend, level: json.country["United Kingdom"].trend, val: json.country["United Kingdom"].total, url: "/sites/default/files/"+json.country["United Kingdom"].file },
			MDA:	{ fillKey: "", level: "", val: "No data", url: "" },
			UKR:	{ fillKey: "", level: "", val: "No data", url: "" },
			GEO:	{ fillKey: "", level: "", val: "No data", url: "" },
			AZE:	{ fillKey: "", level: "", val: "No data", url: "" },
		};

		optionalCountries = [["HUN","Hungary"],["MDA","Moldova"],["UKR","Ukraine"],["GEO","Georgia"],["AZE","Azerbaijan"]];
		$.each(optionalCountries,function(k,v){
			code = v[0];
			name = v[1];
			if ( json.country[ name ] ) {
				map_data[code] = {
					fillKey: json.country[name].trend,
					level: json.country[name].trend,
					val: json.country[name].total,
					url: "/sites/default/files/"+json.country[name].file
				};
			}
		});
		mapMode = "eu";
		if ( json.country["Azerbaijan"] || json.country["Georgia"] ) {
			mapMode = "wide";
		}

		$("#landscaping-map").html("");
		var trend = { fillKey: "Trend-setter", level: "Trend-setter", val: 400 };
		var begin = { fillKey: "Beginner", level: "Beginner", val: 200 };
		var follo = { fillKey: "Follower", level: "Follower", val: 100 };
		var track = { fillKey: "Fast-tracker", level: "Fast-tracker", val: 500 };
		map = new Datamap({
			element: document.getElementById("landscaping-map"),
			fills: {
				"Beginner": "#FE9900",
				"Follower": "#039ECE",
				"Fast-tracker": "#22377A",
				"Trend-setter": "#FE5D20",
				defaultFill: "#FFFFFF",
			},
			data: map_data,
			geographyConfig: {
				popupTemplate: function(geography, data) {
					if (geography.id === "ALA") {
						return "<div class='hoverinfo "+json.country["Finland"].trend.toLowerCase().replace(/ /g, '-')+"'><strong>Finland</strong>: "+json.country["Finland"].total.toFixed(1)+"%</div>";
					}
					if (geography.id === "HUN" && map_data.HUN.fillKey == "") {
						return "<div class='hoverinfo " + data.level.toLowerCase().replace(/ /g, '-') + "'><strong>" + geography.properties.name + "</strong>: " + data.val + "</div>";
					}
					if (geography.id === "MDA" && map_data.MDA.fillKey !== "") {
						return "<div class='hoverinfo " + data.level.toLowerCase().replace(/ /g, '-') + "'><strong>Moldova</strong>: " + data.val.toFixed(1)+"%</div>";
					}
					if (!data) return;
					if ( year > 2017 ) {
						return "<div class='hoverinfo " + data.level.toLowerCase().replace(/ /g, '-') + "'><strong>" + geography.properties.name + "</strong>: " + data.val.toFixed(1) + "%</div>";
					} else {
						return "<div class='hoverinfo " + data.level.toLowerCase().replace(/ /g, '-') + "'><strong>" + geography.properties.name + "</strong>: " + data.val + "</div>";
					}
				},
				highlightFillColor: function(data) {
					if (!data.val) {
						return "#FFFFFF";
					} else if (data.fillKey === "Beginner") {
						return "rgba(254,153,0,0.8)";
					} else if (data.fillKey === "Follower") {
						return "rgba(3,158,206,0.8)";
					} else if (data.fillKey === "Fast-tracker") {
						return "rgba(34,55,122,0.8)";
					} else if (data.fillKey === "Trend-setter") {
						return "rgba(254,93,32,0.8)";
					}
				},
				highlightBorderColor: "#FFFFFF",
				highlightBorderWidth: 0,
			},
			setProjection: function(element, options) {
				var projection =d3.geo
					.equirectangular()
					.center([ (mapMode !== "wide" ? 15 : 20) , 50])
					.rotate([4.4, 0])
					.scale(
						jQuery(window).width() > 800
						? (mapMode !== "wide" ? 1000 : 800)
						: jQuery(window).width() > 601
							? 750
							: 500)
					.translate([element.offsetWidth / 2, element.offsetHeight / 2]);

				var path = d3.geo.path().projection(projection);
				return { path: path, projection: projection };
			},
			responsive: true,
			done: function(datamap) {
				datamap.svg.selectAll(".datamaps-subunit").on("click", function(geo) {
					if ($(this).data("info")) {
						if ($(this).data("info").url) {
							window.open($(this).data("info").url, "_blank");
						} else {
							alert("There is no factsheet available for this country at present");
						}
					}
				});
			}
		});
		map.legend();
		setTimeout(function(){addMapBubbles(map_data.MLT,map_data.LIE);}, 100);
		map.svg.selectAll(".datamaps-bubble").on("click", function(geo) {
			if ($(this).data("info")) {
				if ($(this).data("info").url) {
					window.open($(this).data("info").url, "_blank");
				} else {
					alert("There is no factsheet available for this country at present");
				}
			}
		});
		/* Aland Islands */
		$("path.ALA, path.FIN").on("mouseover", function() {
			$("path.ALA, path.FIN").css("opacity", "0.8").css("stroke-width", "0");
		}).on("mouseout", function() {
			$("path.ALA, path.FIN").css("opacity","1.0").css("stroke-width", "1px");
		});

	// datatables
		jQuery("#edp3 .dashboard-table table").DataTable({
			info: false,
			order: [],
			paging: false,
			searching: false
		});
}

function addMapBubbles(mt,lt) {
	map.bubbles(
		[
			{ name: "Malta", latitude: 35.94, longitude: 14.38, radius: 10, fillKey: mt.fillKey, val: mt.val, url: mt.url },
			{ name: "Liechtenstein", latitude: 47.17, longitude: 9.56, radius: 10, fillKey: lt.fillKey, val: lt.val, url: lt.url },
		], {
			popupTemplate: function(geo, data) { return "<div class='hoverinfo " + data.fillKey.toLowerCase().replace(/ /g, '-') + "'><strong>" + data.name + "</strong>: " + data.val.toFixed(1) + "%</div>"; }
		}
	);
}

function minusOneOrNoData(val) {
	if ( val !== -1 ) {
		return val;
	} else {
		return "";
	}
}