var runAlready = false;
var cookiesAgreed = false;
var licensingAssistantLoadStart = false;
var licensingAssistantLoadEnd = false;
var searchOrderingStarted = false;
var searchFilters = false;
var scrollPad = 0;
var homeSlickLoaded = false;
$ = jQuery;

(function ($, Drupal, drupalSettings) {

	Drupal.behaviors.myBehavior = {
		attach: function (context, settings) {

			menuExpander($);
			faqExpander($);
			tabToggler($);
			calendarJumper($);
			loadLicensingAssistant($);
			mobileMenu($);
			slickLatestReports($);
			searchFiltersMobile($);
			menuFix($);
			searchSortBy($);

			scrollPad = 10;
			scrollPad = scrollPad + ( $("#toolbar-administration").length == 0 ? 0 : 39);
			scrollPad = scrollPad + ( $("#primary_menu ul.menu:visible").length * 48 );

			if ( !runAlready ) {
				runAlready = true;

				if ( Drupal.eu_cookie_compliance ) {
					cookiesAgreed = Drupal.eu_cookie_compliance.hasAgreed();
				}

				// tell the body we have javascript
				$("body").addClass("js");

				$(window).scroll(function() {

					if ($(this).scrollTop() > 200) {
						$('.btn-back-top').fadeIn(200);
					} else {
						$('.btn-back-top').fadeOut(200);
					}

					if($(window).scrollTop() + $(window).height() > $(document).height() - $("#edp3 #footer_second").innerHeight()) {
						$("#edp3 .btn-back-top").css("bottom", $("#edp3 #footer_second").innerHeight() + 10);
					} else {
						$("#edp3 .btn-back-top").css("bottom", "10px");
					}

				});

				$(".btn-back-top").on("click", function(e) {
					e.preventDefault();
					$("html,body").animate({scrollTop: 0}, 300);
					$(this).blur();
					return false;
				});

				// pad a hash navigation
				window.addEventListener("hashchange", function () {
					console.log(scrollPad);
					window.scrollTo(window.scrollX, window.scrollY - scrollPad);
				});
			}

		}
	};

})(jQuery, Drupal, drupalSettings);


/*
****************************************************************************************************
** Because DRUPAL is now insisting on running everything through it's behaviours wrapper, the jQuery code to connect
** click, change, and other events is often called multiple times. This can cause the code to execute multiple time, depending
** on when it has been added (repeatedly) by Drupal. In order to mitigate this, all "click" functions have been added with a
** call to off("click") first, then on("click") to re-attach the code. Obviously, replace "click" with "change" or any other event.
** The functions below have been taken out of the main Drupal Behaviour block above to hopefully make it easier to tell
** which function does what.
****************************************************************************************************
*/

function menuFix($) {
	// fix for the News menu item not showing as selected with URL parameters
	if ( $("#edp3.news-events--news").length !== 0 && jQuery("#edp3 #primary_menu a.is-active").length == 0 ) {
			$("#edp3 #primary_menu a[data-drupal-link-system-path='news-events/news']").addClass("is-active");
	}
}

function faqExpander($) {
/*
	expands and collapses FAQ elements to show or hide the answers
*/
	$(".faq-question").off("click").on("click",function(e){
		$($(this).parent()).toggleClass("open");
	});
}

function getParameterByName(name) {
	var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
	return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
function loadLicensingAssistant($) {
	if ( ! $("#la-target").length || $("#la-target").text().trim() !== "" || licensingAssistantLoadStart ) {
		return;
	}

	licensingAssistantLoadStart = true;

	$.getScript("https://www.europeandataportal.eu/licensing-assistant/lib.js?q3w05i",function(data,textStatus,jqxhr) {
	    if ( getParameterByName("license_id") ) {
	    	new LicenseAssistant.ShowLicense( jQuery("#la-target") , getParameterByName("license_id") );
	    } else {
	    	new LicenseAssistant.FilterLicenses($("#la-target"));
			$("#la-target").find("h2").replaceWith(function(){ return "<blockquote>"+$(this).html() + "</blockquote>"; });
			$("#edp3 #la-target .search-box button").on("click",function() {
				$("#edp3 #la-target #filter-panel").slideToggle();
			});
		}
	});

	window.showLicense = function(name) {
		document.location.href = "licensing-assistant?license_id=" + encodeURIComponent(name);
	};
	
}

function menuExpander($) {
/*
	expands and hides the second level menu
*/
	var $menuAs = "#edp3 #primary_menu li.menu-item--expanded:not(.menu-item--active-trail) > a";
	var $menuSpans = "#edp3 #primary_menu li.menu-item--expanded:not(.menu-item--active-trail) > span";
	$($menuAs + ", " + $menuSpans).off("click").on("click",function(e){
		e.preventDefault();
		if ( $(this).parent().hasClass("open") ) {
			$("#edp3 #primary_menu .open").removeClass("open").removeClass("menu-item--active-trail");
		} else {
			$("#edp3 #primary_menu .open").removeClass("open").removeClass("menu-item--active-trail");
			$(this).parent().toggleClass("menu-item--active-trail").toggleClass("open");
		}
	});

	$("#primary_menu").off("click").on("click", function(e) {
		e.stopPropagation();
	});
	$(document).off("click").on("click", function(e) {
		$("#primary_menu .open").removeClass("open").removeClass("menu-item--active-trail");
	});
	
	$("#edp3 #primary_menu li.menu-item--expanded.menu-item--active-trail > a").off("click").on("click",function(e){
		e.preventDefault();
		$("#primary_menu .open").removeClass("open").removeClass("menu-item--active-trail");
	});
}

function mobileMenu($) {
/*
	let me navigate responsively
*/
	$("#edp3 .menu--btn").off("click").on("click", function(e){
		e.preventDefault();
		$(this).find("span").toggleClass("fa-times");
		var $menu = $("#edp3 #primary_menu .menu--main > ul.menu")
		$menu.slideToggle();
		$menu.scrollTop = 0;
		$("html,body").animate({ scrollTop: $("#edp3 #header .region-header").height() }, 'normal');
		$("body").toggleClass("overflow-hidden");
	});
}

function tabToggler($) {
/*
	enables the tab behaviour from Bootstrap
*/
	$('[role="tablist"] a').off("click").on('click', function (e) {
		e.preventDefault();
		if ( cookiesAgreed ) {
			setCookie( $(this).parent().parent().attr("id") , $(this).data().target );
		}
		$(this).tab('show');
	})
	$.each( $('[role="tablist"]'),function(k,v) {
		tab = $(v).attr("id");
		cookie = getCookie( tab );
		if ( cookie && ! $("#"+tab+"Content "+cookie).hasClass("show active") ) {
			$("#"+tab+" .active").removeClass("active");
			$("#"+tab+" a[data-target='"+cookie+"']").addClass("active");
			$("#"+tab+"Content .show.active").removeClass("show active");
			$("#"+tab+"Content "+cookie).addClass("show active");
		}
	});
}

function slickLatestReports($) {
	if ( ($("#edp3 #block-promoted-slideshow").length === 0 && $("#edp3 #block-promoted-carousel").length === 0) || homeSlickLoaded ) {
		return;
	}

	if ( $("#edp3 #block-promoted-slideshow").length !== 0 ) {
		var promotedSlideshow = $("#block-promoted-slideshow .view-content").slick({
			autoplay: true,
			autoplaySpeed: 5000,
			dots: true,
			nextArrow: $(".slick-c-next"),
			prevArrow: $(".slick-c-prev")
		});
	} else if ( $("#edp3 #block-promoted-carousel").length !== 0 ) {
		var promotedSlideshow = $("#block-promoted-carousel .view-content").slick({
			autoplay: true,
			autoplaySpeed: 5000,
			dots: true,
			nextArrow: $(".slick-c-next"),
			prevArrow: $(".slick-c-prev")
		});
	}

	$(".slick-c-play").on("click", function() {
		promotedSlideshow.slick("slickPlay");
		$(".slick-c-pause").show();
		$(".slick-c-play").hide();
	});
	$(".slick-c-pause").on("click", function() {
		promotedSlideshow.slick("slickPause");
		$(".slick-c-pause").hide();
		$(".slick-c-play").show();
	});

	homeSlickLoaded = true;
}

function calendarJumper($) {

	if ( $("#edp3 .js-drupal-fullcalendar").length === 0 ) {
		return;
	}

	if ( window.location.hash.replace("#","") !== "" ) {
		target = window.location.hash.replace("#","").split("-");
		targetYear = parseInt(target[0]);
		targetMonth = parseInt(target[1]);

		shown = new Date( $(".js-drupal-fullcalendar").fullCalendar("getDate")._d );

		if ( targetYear !== (1900+shown.getYear()) || targetMonth !== (shown.getMonth()+1) ) {
			$(".js-drupal-fullcalendar").fullCalendar("gotoDate",new Date(targetYear,targetMonth-1,1));
		}

	}

	$(".js-drupal-fullcalendar .fc-prev-button").off("click").on("click",function(){
		$(".js-drupal-fullcalendar").fullCalendar("prev");
 		shown = new Date( $(".js-drupal-fullcalendar").fullCalendar("getDate")._d );
 		window.location.hash = (1900+shown.getYear()) + "-" + (shown.getMonth()+1 < 10 ? "0" : "") + (shown.getMonth()+1);
 	});
	$(".js-drupal-fullcalendar .fc-next-button").off("click").on("click",function(){
		$(".js-drupal-fullcalendar").fullCalendar("next");
 		shown = new Date( $(".js-drupal-fullcalendar").fullCalendar("getDate")._d );
 		window.location.hash = (1900+shown.getYear()) + "-" + (shown.getMonth()+1 < 10 ? "0" : "") + (shown.getMonth()+1);
 	});
	$(".js-drupal-fullcalendar .fc-today-button").off("click").on("click",function(){
		$(".js-drupal-fullcalendar").fullCalendar("today");
 		shown = new Date( $(".js-drupal-fullcalendar").fullCalendar("getDate")._d );
 		window.location.hash = (1900+shown.getYear()) + "-" + (shown.getMonth()+1 < 10 ? "0" : "") + (shown.getMonth()+1);
 	});
}


function searchSortBy($) {
  /*
		change the sort by + sort order fields into one field
  */

  if ($(".form-item-sort-by:visible").length === 0 || searchOrderingStarted) {
    return;
	}

	searchOrderingStarted = true;

  $parent = $(".form-item-sort-by:visible").parents("form");
  $parentId = "#edp3 #content_middle #" + $parent.attr("id");
  // Hide the old sorting
  $($parentId + " .form-item-sort-by, .form-item-sort-order").hide();
  // Create the OrderBy structure
  $($parentId + " .form--inline").prepend(
    '<div class="order-wrap">' +
    '<a href="#" class="btn btn-secondary order text-white text-decoration-none">' +
    '<span class="order-title"></span>: <span class="order-text"></span>' +
    '</a>' +
    '<div class="order-list"></div>' +
    '</div>');

  // Define OrderBy list
  $orderList = $($parentId + " .order-list");
  // Add available filters to the OrderBy list
  $.each($($parentId + " .form-item-sort-by .form-select"), function (k, v) {
    $(v).find("option").each(function () {
      $orderList.append('<button data-val="' + $(this).val() + '-ASC" class="order-rlv-asc">' + $(this).text() + " " + Drupal.t("Ascending") + ' </button>');
      $orderList.append('<button data-val="' + $(this).val() + '-DESC" class="order-rlv-desc">' + $(this).text() + " " + Drupal.t("Descending") + '</button>');
    });
  });
  // Update OrderBy button text
  $($parentId + " .btn.order .order-title").text($($parentId + " .form-item-sort-by label").text());
  $($parentId + " .btn.order .order-text").text($($parentId + " .form-item-sort-by option:selected").text() + " " + $($parentId + " .form-item-sort-order option:selected").text());
  // Create OrderBy button and mirror the selection with the hidden inputs
  $.each($($orderList).find("button"), function (k, v) {
    $(v).on("click", function (e) {
      //e.preventDefault();
      $orderList.toggleClass("order-list__visible");
      var data = $(this).attr("data-val")
      var filter = data.split("-");
      var filter1 = filter[0];
      var filter2 = filter[1];
      filter1 = filter1.toString();
      filter2 = filter2.toString();
      $($parentId + " .form-item-sort-by select").val(filter1);
      $($parentId + " .form-item-sort-order select").val(filter2);
      $($parentId + " .btn.order .order-text").text($(v).text());
    });
  });
  // Show/hide OrderBy list
  $($parentId + " .btn.order").on("click", function (e) {
    e.preventDefault();
    e.stopPropagation();
    $orderList.toggleClass("order-list__visible");
  });
  // Hide OrderBy list if visible when clicking ouside
  $(document.body).on("click", function (e) {
    if ($orderList.hasClass("order-list__visible")) {
      $orderList.removeClass("order-list__visible");
    }
  });
  // Override the submit form button search input
  $($parentId + " #edit-term").keypress(function(e) {
    if (e.keyCode == 13) {
			e.preventDefault();
      $($parentId + " #edit-submit-search").click();
    }
  });

  $($parentId + " .form--inline").prepend(
    '<div class="domain-wrap">' +
    '<div class="domain">' +
    '<div class="subtitle">' + Drupal.t('Search Domain:') + '</div>' +
    '<div class="radiobutton">' +
    '<input id="domain-1" type="radio" name="searchdomain" value="site" checked> <label for="domain-1">' + Drupal.t("Site Content") + '</label>' +
    '</div>' +
    '<div class="radiobutton">' +
    '<input id="domain-2" type="radio" name="searchdomain" value="data"> <label for="domain-2">' + Drupal.t("Datasets") + '</label>' +
    '</div>' +
    '</div>' +
		'</div>');
		

}


function searchFiltersMobile($) {

	/* Collapse filters on mobile */

  if (searchFilters) {
    return;
	}

	searchFilters = true;

	function collapseSearch() {
		if(!$(".btn-filter-search .fas").hasClass("fa-times")) {
			setTimeout(function(){ 
				$("#edp3.search .btn-filter-search + #block-search-form-left, #edp3.search .domain-wrap, #edp3.search .order-wrap").slideUp();
			}, 0);
		}
	}

	$("#edp3.search #content_left .region-content-left, #edp3:not(.search) .search-results").prepend('<a href="#" class="btn-filter-search"><span class="fas fa-filter"></span></a>');

	if (window.innerWidth <= 991) {
		collapseSearch();
	}
	$(window).on('resize', function(){
		if (window.innerWidth <= 991) {
			collapseSearch();
		}
	});

	$(".btn-filter-search").on("click", function(e) {
		e.preventDefault();
		$(this).find(".fas").toggleClass("fa-times");
		$("#edp3.search .btn-filter-search + #block-search-form-left, #edp3.search .domain-wrap, #edp3.search .order-wrap, #edp3:not(.search) .search-results .view-filters").slideToggle();
	});

}


function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}