kubectl create secret generic mysql-pass --from-literal=password=owIpWVTYi2 --namespace=ppe

kubectl exec -it edp-mysql-5f574d7648-wxt8q -n ppe -- mysql -uroot -powIpWVTYi2 -f drupal < 20200227-PM2.sql

eksctl utils update-cluster-logging --region=eu-central-1 --cluster=EDP-PPE

docker build -t registry.gitlab.ppe.iisahost.net/edp/drupal/drupal:latest -f ./docker/drupal/Dockerfile .
docker build -t registry.gitlab.ppe.iisahost.net/edp/drupal/nginx:latest -f ./docker/nginx/Dockerfile .

docker push registry.gitlab.ppe.iisahost.net/edp/drupal/drupal:latest
docker push registry.gitlab.ppe.iisahost.net/edp/drupal/nginx:latest

php -d memory_limit=-1 \ProgramData\ComposerSetup\bin\composer.phar require drush/drush

php -d memory_limit=-1 /usr/bin/composer update --dry-run "drupal/*"